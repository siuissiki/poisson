#--------1---------2---------3---------4---------5---------6---------7---------8---------9---------0
#--
#-- Makefile
#--


.SUFFIXES:
.SUFFIXES: .o .d .f90

FC      := mpif90
FFLAGS  := 
LDFLAGS := 
PREPROC :=
#FDEBUG  := -cpp -Ddebug -Wall -fbounds-check -O -Wuninitialized -ffpe-trap=invalid,zero,overflow -fbacktrace -g 
FDEBUG  := -fpp -Ddebug -check all -warn all -gen_interfaces -fpe0 -ftrapuv -traceback -g


#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0


#-- Object file name
OBJS  := types.o \
				 mpi.o \
				 consts.o \
				 variables.o \
				 read_input.o  \
				 calc.o  \
				 cg.o

#-- Executive name
EXEC := out	


#-- File search path
VPATH = ./:./src/:./lib/


#--
.PHONY: all clean


#--
all: $(EXEC)


#-- Link command line to generate executive
$(EXEC): $(OBJS)
	$(FC) $(FFLAGS) $(FDEBUG) -o $@ $^ $(LDFLAGS)

#--
%.o: %.f90
	$(FC) $(FFLAGS) $(FDEBUG) $(PREPROC) -c $<


%.o: %.f
	$(FC) $(FFLAGS) $(FDEBUG) $(PREPROC) -c $<


clean: 
	rm -f $(EXEC) *.o *.mod *.txt *~


#--
#-- end of Makefile
#--
#---+----1----+----2----+----3----+----4----+----5----+----6----+----7----+----8----+----9----+----0
