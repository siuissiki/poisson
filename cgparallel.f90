module types
  implicit none
  type int4
    integer :: g, x, y, z
  end type int4
end module types
program main
  use types
  implicit none
  include "mpif.h"
  type(int4) num_proc
  type(int4) my_rank
  integer ierr
  call mpi_init(ierr)
  call mpi_comm_size(MPI_COMM_WORLD, num_proc%g, ierr)
  call mpi_comm_rank(mpi_comm_world, my_rank%g, ierr)
  if (my_rank%g == 0) call read_input(num_proc)
  call mpi_bcast (num_proc,4,mpi_integer,0,mpi_comm_world,ierr)
  call MPI_Barrier( MPI_COMM_WORLD , ierr)
  my_rank%x = mod(my_rank%g, num_proc%x)
  my_rank%y = mod(my_rank%g/num_proc%x, num_proc%y)-my_rank%x/num_proc%x
  my_rank%z = (my_rank%g-my_rank%x-my_rank%y*num_proc%x)/(num_proc%x*num_proc%y)
  print *, my_rank
  call MPI_FINALIZE(ierr)
contains
  subroutine mpi_setup
    use types
    implicit none
    type(int4) :: num_proc
    type(int4) :: my_rank
    integer :: ierr
  end subroutine mpi_setup
  subroutine read_input(num_proc)
    use types
    implicit none
    type(int4) num_proc
    open(17, file='inputfile', status='old', err=99)
    read(17,*) num_proc%g, num_proc%x, num_proc%y, num_proc%z
    close(17)
    return
99  write(*,*) "Input file is not found"
    stop
  end subroutine read_input

end program main