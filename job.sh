# 2017/04/21
#!/bin/csh
#$ -cwd -pe mpi 2
#$ -V
setenv OMP_NUM_THREADS 1
#setenv OPAL_PREFIX /usr/local/openmpi-2.1.0

cp $USER.$PE.$JOB_ID.2 hosts
foreach bbbb (`cat hosts`)
set ncpcpu = (`echo $bbbb | cut -c 8-9`)
cp $USER.$PE.$JOB_ID.1 hosts
set TEST = ""
foreach aaaa (`cat hosts`)
@ I++
set TEST = (${TEST} `echo $aaaa`)
end
rm hosts
set i=0
while ( $i < $NSLOTS )
@ i = $i + $OMP_NUM_THREADS
echo $TEST[$i] >> hosts
end
@ i = $NSLOTS / $OMP_NUM_THREADS
@ NPS = $ncpcpu / $OMP_NUM_THREADS / 2

if ( $NPS >= 1 ) then
mpirun -np $i -machinefile hosts -npersocket $NPS ./out
endif
# only for 2 cpu/process
if ( $NPS < 1 ) then
mpirun -np $i -machinefile hosts -bind-to none ./out
endif

