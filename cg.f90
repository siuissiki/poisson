program main
  use types
  use consts
  use variables
  use read
  use calc
  use mpi
  implicit none
  character(8) filename
! rho parameter
  call mpi_init(ierr)
  call mpi_comm_size(mpi_comm_world, num_proc%g, ierr)
  !mpi_abort
  call mpi_comm_rank(mpi_comm_world, my_rank%g, ierr)
  if(my_rank%g == 0) call read_input(num_proc, n, d, length, ierr)
  call broadcast(num_proc, n, d, length, ierr)
  allocate(rho(n%x,n%y,n%z))
  allocate(phi(n%x,n%y,n%z))
  allocate(r(n%x,n%y,n%z))
  allocate(f(n%x,n%y,n%z))
  allocate(Ax(n%x,n%y,n%z))
  phi = 0
  call get_my_rank(num_proc, my_rank)
  call rho_init(rho,z,a,b,d,length)
  !write(*,*) "rho_init"
  !call cg(phi, rho, d, 40)
  !write(*,*) "cg"
  call output(rho, d%x, d%y, d%z, length%x, length%y, length%z, num_proc)
  call MPI_FINALIZE(ierr)

contains
  subroutine rho_init(rho, z, a, b, d, length)
    use types
    implicit none
    double precision, intent(inout) :: rho(:,:,:)
    double precision, intent(in) :: z(:)
    double precision, intent(in) :: a(:,:)
    double precision, intent(in) :: b(:)
    type(dp3), intent(in) :: length
    type(dp3), intent(in) :: d
    type(int3) n
    integer i,j,k,l,zmax,ii,jj,kk
    double precision, parameter :: pi = 3.141592d0
    n%x = size(rho(:,1,1))
    n%y = size(rho(1,:,1))
    n%z = size(rho(1,1,:))
    zmax = size(z)
    rho = 0.0d0
    do k = 1, n%z
    do j = 1, n%y
    do i = 1, n%x
      ii = i + my_rank%x*n%x
      jj = j + my_rank%y*n%y
      kk = k + my_rank%z*n%z
      do l = 1, zmax
        rho(i, j, k) = rho(i, j, k) + z(l) * (b(l)/pi) ** (3.0/2.0) * &
        exp(-b(l)*((ii*d%x-length%x/2.0-a(1,l))**2+(jj*d%y-length%y/2.0-a(2,l))**2+(kk*d%z-length%z/2.0-a(3,l))**2.0))
      end do
      rho(i,j,k) = rho(i,j,k) - 2.0/(n%x*n%y*n%z)/(d%x*d%y*d%z)
    end do
    end do
    end do
  end subroutine
  subroutine cg(x, b, d, loop_max)
    implicit none
    double precision, allocatable, dimension(:,:,:) :: x, b, r
    double precision, allocatable, dimension(:,:,:) :: Ax, p, Ap
    type(dp3) d
    type(int3) n
    integer i,j,k,loop,loop_max
    double precision rr,beta,pAp,alpha,eps
    eps = 10.0d0**(-5.0d0)
    n%x = size(x(1,1,:))
    n%y = size(x(1,:,1))
    n%z = size(x(:,1,1))
    beta = 0.0d0
    allocate(Ax(n%x,n%y,n%z))
    allocate(Ap(n%x,n%y,n%z))
    allocate(p(n%x,n%y,n%z))
    allocate(r(n%x,n%y,n%z))
    !ラプラシアン計算
      Ax = laplacian(x, d, 4)
      !初期残差計算
      do k = 1, n%z
      do j = 1, n%y
      do i = 1, n%x
        r(i, j, k) = 4.0d0*pi*b(i, j, k) - Ax(i, j, k)
      end do
      end do
      end do
      rr = norm(r)
      do loop = 1, loop_max
      !修正係数計算
        do k = 1, n%z
        do j = 1, n%y
        do i = 1, n%x
          p(i, j, k) = r(i, j, k) + beta*p(i, j, k)
        end do
        end do
        end do
        Ap = laplacian(p, d, 4)
        pAp = dot(p, Ap)
        alpha = rr/pAp
        do k = 1, n%z ! phi, r計算
        do j = 1, n%y
        do i = 1, n%x
          x(i, j, k) = x(i, j, k) + alpha*p(i, j, k)
          r(i, j, k) = r(i, j, k) - alpha*Ap(i, j, k)
        end do
        end do
        end do
        rr = norm(r)
        beta = rr/(alpha*pAp)
        if (rr < eps) exit
      end do
  end subroutine
  subroutine output(phi, dx, dy, dz, xlength, ylength, zlength, num_proc)
    use types
    integer nx, ny, nz, wfile, ix, iy, iz, na, ierr, req, i
    type(int3) irank
    double precision dx, dy, dz, xlength, ylength, zlength
    double precision, allocatable, dimension(:,:,:) :: phi
    double precision, allocatable, dimension(:,:,:) :: phi_global
    character(40) :: filename
    type(int4) :: num_proc
    nx = size(phi(:,1,1))
    ny = size(phi(1,:,1))
    nz = size(phi(1,1,:))
    allocate(phi_global(nx*num_proc%x, ny*num_proc%y, nz*num_proc%z))
    if(num_proc%g/=1) then
      do i = 1, num_proc%g-1
        irank%x = mod(i, num_proc%x)
        irank%y = mod(i/num_proc%x, num_proc%y)-irank%x/num_proc%x
        irank%z = (i-irank%x-irank%y*num_proc%x)/(num_proc%x*num_proc%y)
        write(*,*) "irank", irank
        if(my_rank%g==0) then
          call mpi_irecv(phi_global((irank%x)*nx+1, (irank%y)*ny+1, (irank%z)*nz+1),nx*ny*nz,mpi_double_precision, &
          i,0,mpi_comm_world,req,ierr)
        else if(my_rank%g==i) then
          call mpi_isend(phi,nx*ny*nz,mpi_double_precision,0,0,mpi_comm_world,req,ierr)
        end if
        call MPI_Barrier( MPI_COMM_WORLD , ierr)
      end do
      !if(my_rank%g==0) then
        !do iz = 1, num_proc%z
        !do iy = 1, num_proc%y
        !do ix = 2, num_proc%x
        !  call mpi_irecv(phi_global((ix-1)*nx+1, (iy-1)*ny+1, (iz-1)*nz+1),nx*ny*nz,mpi_double_precision, &
        !  ix-1+(iy-1)*num_proc%x+(iz-1)*num_proc%x*num_proc%y,0,mpi_comm_world,req,ierr)
        !end do
        !end do
        !end do
      !else
      !  call mpi_isend(phi,nx*ny*nz,mpi_double_precision,0,0,mpi_comm_world,req,ierr)
      !end if
    end if
    write(*,*) nx, ny, nz
    if(my_rank%g==0) phi_global(1:nx,1:ny,1:nz) = phi(1:nx,1:ny,1:nz)
    call MPI_Barrier( MPI_COMM_WORLD , ierr)
    if(my_rank%g==0) then
      filename = 'phi.xsf'
      wfile = 10
      open(wfile, file = filename )
      write(wfile,*) 'ATOMS'
      do na = 1, 2
        write(wfile,'(i3,3x,6f13.8)') 1, (-xlength/2-a(1,na))*bohr2ang, (-ylength/2-a(2,na))*bohr2ang, (-zlength/2-a(3,na))*bohr2ang
      end do
      write(wfile,*) 'BEGIN_BLOCK_DATAGRID_3D'
      write(wfile,*) ' my_first_example_of_3D_datagrid'
      write(wfile,*) ' BEGIN_DATAGRID_3D_this_is_3Dgrid#1'
      write(wfile,'(3i6)') nx*num_proc%x, ny*num_proc%y, nz*num_proc%z
      write(wfile,'(3f9.5)') (-xlength+0.5d0*dx)*bohr2ang,(-ylength+0.5d0*dy)*bohr2ang,(-zlength+0.5d0*dz)*bohr2ang
      write(wfile,'(3f9.5)') (nx*num_proc%x*dx)*bohr2ang, 0.0d0, 0.0d0
      write(wfile,'(3f9.5)') 0.0d0, (ny*num_proc%y*dy)*bohr2ang,0.0d0
      write(wfile,'(3f9.5)') 0.0d0,0.0d0,(nz*num_proc%z*dz)*bohr2ang
      write(wfile,'(80f9.5)') (((phi_global(ix,iy,iz),ix=1,nx*num_proc%x),iy=1,ny*num_proc%y),iz=1,nz*num_proc%z)
      write(wfile,*) ' END_DATAGRID_3D'
      write(wfile,*) 'END_BLOCK_DATAGRID_3D'
      close(wfile)
      call system("open -a VESTA output/phi.xsf")
    end if
  end subroutine
end program
