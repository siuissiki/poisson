module mpi
  implicit none
  include "mpif.h"
contains
  subroutine get_my_rank(num_proc, my_rank)
    use types
    implicit none
    type(int4) :: num_proc
    type(int4) :: my_rank
    my_rank%x = mod(my_rank%g, num_proc%x)
    my_rank%y = mod(my_rank%g/num_proc%x, num_proc%y)-my_rank%x/num_proc%x
    my_rank%z = (my_rank%g-my_rank%x-my_rank%y*num_proc%x)/(num_proc%x*num_proc%y)
    print *, my_rank
  end subroutine get_my_rank
  subroutine broadcast(num_proc, n, d, length, ierr)
    use types
    implicit none
    type(int4) num_proc
    type(int3) n
    type(dp3) length, d
    integer ierr
    call MPI_BCAST (num_proc,4,mpi_integer,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST (n,3,mpi_integer,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST (d,3,mpi_double_precision,0,MPI_COMM_WORLD,ierr)
    call MPI_BCAST (length,3,mpi_double_precision,0,MPI_COMM_WORLD,ierr)
  end subroutine
end module mpi