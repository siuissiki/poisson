module types
  type int3
    integer :: x, y, z
  end type int3
  type int4
    integer :: g, x, y, z
  end type int4
  type dp3
    double precision :: x, y, z
  end type dp3
end module types