module consts
  implicit none
! 物理定数
  double precision, parameter :: pi = 3.141592d0
  double precision, parameter :: bohr2ang = 0.5291772083
end module consts