module read
  use mpi
  implicit none
contains
  subroutine read_input(num_proc, n, d, length, ierr)
    use types
    type(int4) num_proc
    type(int3) n
    type(dp3) length, d
    integer ierr
    open(17, file='input', status='old', err=99)
    read(17,*)
    read(17,*) num_proc%x, num_proc%y, num_proc%z
    read(17,*)
    read(17,*) n%x, n%y, n%z
    read(17,*)
    read(17,*) length%x, length%y, length%z
    if(num_proc%g /= num_proc%x * num_proc%y * num_proc%z) then
      write(*,*) "num_proc%g \= num_proc%x * num_proc%y * num_proc%z"
      call mpi_abort(mpi_comm_world, ierr)
    end if
    d%x = length%x / n%x
    d%y = length%y / n%y
    d%z = length%z / n%z
! num_procでnが割り切れるかどうか確認
    if(mod(n%x, num_proc%x)/=0) then
      write(*,*) "mod(n%x, num_proc%x)/=0"
      call mpi_abort(mpi_comm_world, ierr)
    end if
    if(mod(n%y, num_proc%y)/=0) then
      write(*,*) "mod(n%y, num_proc%y)/=0"
      call mpi_abort(mpi_comm_world, ierr)
    end if
    if(mod(n%z, num_proc%z)/=0) then
      write(*,*) "mod(n%z, num_proc%z)/=0"
      call mpi_abort(mpi_comm_world, ierr)
    end if
! プロセスごとのグリッド数計算
    n%x = n%x / num_proc%x
    n%y = n%y / num_proc%y
    n%z = n%z / num_proc%z
    return
99  write(*,*) "Input file is not found"
    stop
  end subroutine
end module read