module calc
  implicit none
contains
  function diff(A,B) result(ret)
    double precision, dimension(:,:,:), intent(in) :: A
    double precision, dimension(:,:,:), intent(in) :: B
    double precision :: ret
    integer nx,ny,nz,i,j,k
    nx = size(A(1,1,:))
    ny = size(A(1,:,1))
    nz = size(A(:,1,1))
    ret = 0.0d0
    do k = 1, nz
    do j = 1, ny
    do i = 1, nx
      ret = ret + abs(A(i,j,k)-B(i,j,k))
    end do
    end do
    end do
    ret = ret/nx/ny/nz
    
    return
  end function

  function dot(x1,x2) result(ret)
    implicit none
    double precision, dimension(:,:,:) :: x1,x2
    double precision :: ret
    integer nx,ny,nz,i,j,k
    nx = size(x1(1,1,:))
    ny = size(x1(1,:,1))
    nz = size(x1(:,1,1))
    ret = 0.0d0
    do k = 1, nz
    do j = 1, ny
    do i = 1, nx
      ret = ret + x1(i, j, k) * x2(i, j, k)
    end do
    end do
    end do

    return
  end function

  function norm(x) result(ret)
    implicit none
    double precision, intent(inout), dimension(:,:,:) :: x
    double precision :: ret
    integer :: nx,ny,nz,i,j,k
    nx = size(x(1,1,:))
    ny = size(x(1,:,1))
    nz = size(x(:,1,1))
    ret = 0.0d0
    do k = 1, nz
    do j = 1, ny
    do i = 1, nx
      ret = ret + x(i, j, k) ** 2
    end do
    end do
    end do

    return
  end function

  function laplacian(ivec, d, order) result(ret)
    use types
    implicit none
    double precision, dimension(:,:,:), intent(in) :: ivec
    type(dp3), intent(in) :: d
    double precision, allocatable, dimension(:,:,:) :: ret
    type(int3) n
    integer order
    integer i,j,k
    double precision, allocatable, dimension(:,:,:) :: subvec
    double precision, dimension(order+1) :: C
    n%x = size(ivec(1,1,:))
    n%y = size(ivec(1,:,1))
    n%z = size(ivec(:,1,1))
    allocate(ret(n%x,n%y,n%z))
    allocate(subvec(-order:n%x+order, -order:n%y+order, -order:n%z+order))
    subvec = 0.0d0
    subvec(1:n%x, 1:n%y, 1:n%z) = ivec
    if (order == 1) then
      C(1) = -2.0d0
      C(2) = 1.0d0
      do k = 1, n%z
      do j = 1, n%y
      do i = 1, n%x
        ret(i, j, k) = -1.0d0*(&
        (C(2)*subvec(i-1,j,k)+C(1)*subvec(i,j,k)+C(2)*subvec(i+1,j,k))/(d%x**2)+&
        (C(2)*subvec(i,j-1,k)+C(1)*subvec(i,j,k)+C(2)*subvec(i,j+1,k))/(d%y**2)+&
        (C(2)*subvec(i,j,k-1)+C(1)*subvec(i,j,k)+C(2)*subvec(i,j,k+1))/(d%z**2))
      end do
      end do
      end do
    else if (order == 4) then
      C(1) = -205.0d0/72.0d0
      C(2) = 8.0d0/5.0d0
      C(3) = -1.0d0/5.0d0
      C(4) = 8.0d0/315.0d0
      C(5) = -1.0d0/560.0d0
      do k = 1, n%z
      do j = 1, n%y
      do i = 1, n%x
        ret(i, j, k) = -1.0d0*(&
        (C(5)*subvec(i-4,j,k)+C(4)*subvec(i-3,j,k)+C(3)*subvec(i-2,j,k)+C(2)*subvec(i-1,j,k)+C(1)*subvec(i,j,k)+&
        C(2)*subvec(i+1,j,k)+C(3)*subvec(i+2,j,k)+C(4)*subvec(i+3,j,k)+C(5)*subvec(i+4,j,k))/(d%x**2)+&
        (C(4)*subvec(i,j-3,k)+C(3)*subvec(i,j-2,k)+C(2)*subvec(i,j-1,k)+C(1)*subvec(i,j,k)+&
        C(2)*subvec(i,j+1,k)+C(3)*subvec(i,j+2,k)+C(4)*subvec(i,j+3,k)+C(5)*subvec(i,j+4,k))/(d%y**2)+&
        (C(5)*subvec(i,j,k-4)+C(4)*subvec(i,j,k-3)+C(3)*subvec(i,j,k-2)+C(2)*subvec(i,j,k-1)+C(1)*subvec(i,j,k)+&
        C(2)*subvec(i,j,k+1)+C(3)*subvec(i,j,k+2)+C(4)*subvec(i,j,k+3)+C(5)*subvec(i,j,k+4))/(d%z**2))
      end do
      end do
      end do
    end if

    return
  end function
end module