module variables
  use types
  implicit none
  double precision, allocatable, dimension(:,:,:) :: rho
  double precision, allocatable, dimension(:,:,:) :: phi
  double precision, allocatable, dimension(:,:,:) :: r
  double precision, allocatable, dimension(:,:,:) :: f
  double precision, allocatable, dimension(:,:,:) :: Ax
  double precision, dimension(2), parameter :: z = (/1.0d0, 1.0d0/)
  double precision, dimension(2) :: b = (/1.0d0, 1.0d0/)
  double precision, dimension(3, 2) :: a = reshape((/0.0d0, 0.0d0, -1.0d0, 0.0d0, 0.0d0, 1.0d0/), shape=(/3, 2/))
  double precision rr
  integer ierr
  type(int4) :: my_rank, num_proc
  type(int3) :: n
  type(dp3) :: length, d
end module